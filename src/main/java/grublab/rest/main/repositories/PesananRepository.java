package grublab.rest.main.repositories;

import grublab.rest.main.models.Customer;
import grublab.rest.main.models.Pesanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PesananRepository extends JpaRepository<Pesanan, String> {

    Iterable<Pesanan> getAllByCustomer(Customer customer);

}
