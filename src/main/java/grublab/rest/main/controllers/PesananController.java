package grublab.rest.main.controllers;

import grublab.rest.main.models.Pesanan;
import grublab.rest.main.services.PesananService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1/orders")
public class PesananController {

    @Autowired
    private PesananService pesananService;

    @Timed("orders_getall")
    @GetMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Pesanan>> getAllPesanan() {
        return ResponseEntity.ok(pesananService.getAllPesanan());
    }

    @Timed("orders_getByIdLine")
    @GetMapping(path = "/{idLine}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Pesanan>> getCustomerHistoryByIdLine(@PathVariable(value = "idLine") String idLine) {
        return ResponseEntity.ok(pesananService.getPesananByIdLine(idLine));
    }

    @Timed("orders_create")
    @PostMapping(path = "/{idLine}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Pesanan> createCustomerHistory(
            @PathVariable(value = "idLine") String idLine,
            @RequestBody Pesanan pesanan) {
        return ResponseEntity.ok(pesananService.createPesanan(idLine, pesanan));
    }

}