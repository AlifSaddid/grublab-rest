package grublab.rest.main.controllers;

import grublab.rest.main.services.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/statistics")
public class StatisticController {

    @Autowired
    public StatisticService statisticService;

    @GetMapping("/pendapatan/current-month/")
    @ResponseBody
    public ResponseEntity<Integer> getPendapatanBulanIni() {
        return ResponseEntity.ok(statisticService.getPendapatanBulanIni());
    }

    @GetMapping("/pendapatan/total/")
    @ResponseBody
    public ResponseEntity<Integer> getPendapatanTotal() {
        return ResponseEntity.ok(statisticService.getPendapatanTotal());
    }

}
