package grublab.rest.main.controllers;

import grublab.rest.main.models.Customer;
import grublab.rest.main.services.CustomerService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Timed("customers_create")
    @PostMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.createCustomer(customer));
    }

    @Timed("customers_getone")
    @GetMapping(path = "/{idLine}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Customer> getCustomerByIdLine(@PathVariable(value = "idLine") String idLine) {
        Customer customer = customerService.getCustomerByIdLine(idLine);
        if(customer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(customer);
    }

    @Timed("customers_update")
    @PutMapping(path = "/{idLine}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Customer> updateCustomerByIdLine(@RequestBody Customer customer, @PathVariable(value = "idLine") String idLine) {
        return ResponseEntity.ok(customerService.updateCustomerPoint(customer, idLine));
    }

    @Timed("customers_getall")
    @GetMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Customer>> getAllCustomers(){
        return ResponseEntity.ok(customerService.getAllCustomers());
    }
}