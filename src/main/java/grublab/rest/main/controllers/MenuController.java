package grublab.rest.main.controllers;

import grublab.rest.main.models.Menu;
import grublab.rest.main.services.MenuService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1/menus")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @Timed("menu_create")
    @PostMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Menu> createMenu(@RequestBody Menu menu) {
        return ResponseEntity.ok(menuService.createMenu(menu));
    }

    @Timed("menu_getall")
    @GetMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Menu>> getAllMenus() {
        return ResponseEntity.ok(menuService.getAllMenus());
    }

    @Timed("menu_getone")
    @GetMapping(path = "/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Menu> getMenuByName(@PathVariable(value = "name") String name) {
        Menu menu = menuService.getMenuByName(name);

        if(menu == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(menu);
    }

    @Timed("menu_delete")
    @PostMapping(path = "/{menuId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Menu> deleteMenuById(@PathVariable(value = "menuId") long menuId) {
        menuService.deleteMenuById(menuId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Timed("menu_update")
    @PutMapping(path = "/{menuId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Menu> updateMenuById(@PathVariable(value = "menuId") long menuId, @RequestBody Menu menu) {
        return ResponseEntity.ok(menuService.updateMenuById(menu, menuId));
    }
}