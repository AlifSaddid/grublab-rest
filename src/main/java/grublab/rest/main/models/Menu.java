package grublab.rest.main.models;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "menu")
@Data
@NoArgsConstructor
@Setter
@Getter
public class Menu implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "menu_id", updatable = false, nullable = false)
    private long menuId;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private int price;

    @ManyToMany(mappedBy = "menus")
    @JsonIgnore
    private List<Pesanan> orders;

    public void addOrders(Pesanan pesanan) {
        this.orders.add(pesanan);
    }
}
