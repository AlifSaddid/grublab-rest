package grublab.rest.main.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "pesanan")
@Data
@NoArgsConstructor
@Setter
@Getter
public class Pesanan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pesanan_id", updatable = false, nullable = false)
    private long pesananId;

    @Column(name = "date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;

    @Column(name = "promo")
    private String promo;

    @Column(name = "display_name")
    private String displayName;

    @ManyToOne
    @JoinColumn(
            name = "id_line",
            referencedColumnName = "id_line"
    )
    @JsonBackReference
    private Customer customer;

    @ManyToMany(cascade = {
            CascadeType.MERGE
    }, fetch = FetchType.LAZY)
    @CollectionId(
            columns = @Column(name = "pesanan_menu_id"),
            type = @Type(type = "long"),
            generator = "sequence"
    )
    @JoinTable(
            name = "pesanan_menu",
            joinColumns = @JoinColumn(name = "pesanan_id"),
            inverseJoinColumns = @JoinColumn(name = "menu_id")
    )
    private List<Menu> menus = new ArrayList<>();

    public void addMenu(Menu menu) {
        this.menus.add(menu);
    }
}