package grublab.rest.main.services;

public interface StatisticService {

    public Integer getPendapatanBulanIni();
    public Integer getPendapatanTotal();

}
