package grublab.rest.main.services;

import grublab.rest.main.models.Customer;

import java.util.List;

public interface CustomerService {

    Customer getCustomerByIdLine(String name);
    Customer createCustomer(Customer customer);
    Customer updateCustomerPoint(Customer customer, String idLine);
    List<Customer> getAllCustomers();
}
