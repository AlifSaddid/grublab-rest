package grublab.rest.main.services;

import grublab.rest.main.models.Menu;
import grublab.rest.main.repositories.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepository menuRepository;

    @Override
    public Menu getMenuByName(String name) {
        return menuRepository.findByName(name);
    }

    @Override
    public Menu createMenu(Menu menu) {
        menuRepository.save(menu);
        return menu;
    }

    @Override
    public Iterable<Menu> getAllMenus() {
        return menuRepository.findAll();
    }
    
    @Override
    public void deleteMenuById(long menuId) {
        menuRepository.deleteById(menuId);
    }

    @Override
    public Menu updateMenuById(Menu menu, long menuId) {
        menu.setMenuId(menuId);
        menuRepository.save(menu);
        return menu;
    }

}
