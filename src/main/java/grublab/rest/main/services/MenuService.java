package grublab.rest.main.services;

import grublab.rest.main.models.Menu;

public interface MenuService {

    Menu getMenuByName(String name);

    Menu createMenu(Menu menu);

    Iterable<Menu> getAllMenus();

    void deleteMenuById(long menuId);

    Menu updateMenuById(Menu menu, long menuId);

}
