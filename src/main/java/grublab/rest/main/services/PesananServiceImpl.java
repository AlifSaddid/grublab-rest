package grublab.rest.main.services;

import grublab.rest.main.models.Customer;
import grublab.rest.main.models.Menu;
import grublab.rest.main.models.Pesanan;
import grublab.rest.main.repositories.MenuRepository;
import grublab.rest.main.repositories.PesananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PesananServiceImpl implements PesananService {

    @Autowired
    private PesananRepository pesananRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private MenuService menuService;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Pesanan createPesanan(String idLine, Pesanan pesanan) {
        Customer customer = customerService.getCustomerByIdLine(idLine);
        pesanan.setCustomer(customer);
        pesanan.setDate(LocalDateTime.now());

        List<Menu> menus = new ArrayList<>();
        for (Menu menu : pesanan.getMenus()) {
            menus.add(menuService.getMenuByName(menu.getName()));
        }

        pesanan.setMenus(menus);
        pesananRepository.save(pesanan);

        return pesanan;
    }

    @Override
    public Iterable<Pesanan> getPesananByIdLine(String idLine) {
        Customer customer = customerService.getCustomerByIdLine(idLine);
        return pesananRepository.getAllByCustomer(customer);
    }

    @Override
    public Iterable<Pesanan> getAllPesanan() {
        return pesananRepository.findAll();
    }
}
