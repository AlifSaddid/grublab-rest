package grublab.rest.main.services;

import grublab.rest.main.models.Menu;
import grublab.rest.main.models.Pesanan;
import grublab.rest.main.repositories.PesananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    PesananRepository pesananRepository;

    @Override
    public Integer getPendapatanBulanIni() {
        List<Pesanan> pesanans = pesananRepository.findAll();
        Integer total = 0;
        for (Pesanan pesanan : pesanans) {
            LocalDateTime dateOrder = pesanan.getDate();
            LocalDateTime dateNow = LocalDateTime.now();
            if (dateOrder.getMonth().equals(dateNow.getMonth()) && dateOrder.getYear() == dateNow.getYear()) {
                for (Menu menu : pesanan.getMenus()) {
                    total += menu.getPrice();
                }
            }
        }
        return total;
    }

    @Override
    public Integer getPendapatanTotal() {
        List<Pesanan> pesanans = pesananRepository.findAll();
        Integer total = 0;
        for (Pesanan pesanan : pesanans) {
                for (Menu menu : pesanan.getMenus()) {
                    total += menu.getPrice();
            }
        }
        return total;
    }
}
