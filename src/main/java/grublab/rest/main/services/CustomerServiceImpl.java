package grublab.rest.main.services;

import grublab.rest.main.models.Customer;
import grublab.rest.main.repositories.CustomerRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Setter
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer getCustomerByIdLine(String name) {
        return customerRepository.findByIdLine(name);
    }

    @Override
    public Customer createCustomer(Customer customer){
        customerRepository.save(customer);
        return customer;
    }

    @Override
    public Customer updateCustomerPoint(Customer customer, String idLine) {
        Customer recentCustomer = customerRepository.findByIdLine(idLine);
        recentCustomer.setPoint(customer.getPoint());
        customerRepository.save(recentCustomer);
        return recentCustomer;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }
}
