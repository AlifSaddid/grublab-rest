package grublab.rest.main.services;

import grublab.rest.main.models.Pesanan;

public interface PesananService {

    public Pesanan createPesanan(String idLine, Pesanan pesanan);

    public Iterable<Pesanan> getPesananByIdLine(String idLine);

    public Iterable<Pesanan> getAllPesanan();

}
