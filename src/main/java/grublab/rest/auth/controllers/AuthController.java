package grublab.rest.auth.controllers;

import grublab.rest.auth.models.Admin;
import grublab.rest.auth.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthController {

    private AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/register")
    public void register(@RequestBody Admin admin) {
        authService.register(admin);
    }

    @PostMapping("/auth")
    public ResponseEntity<Admin> getAdmin(HttpServletRequest request, HttpServletResponse response) {
        Admin admin = authService.convertTokenToAdmin(request);

        if (admin == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        return ResponseEntity.ok(admin);
    }

}
