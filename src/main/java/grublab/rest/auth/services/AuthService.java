package grublab.rest.auth.services;

import grublab.rest.auth.models.Admin;

import javax.servlet.http.HttpServletRequest;

public interface AuthService {

    public void register(Admin admin);

    public Admin convertTokenToAdmin(HttpServletRequest request);

}
