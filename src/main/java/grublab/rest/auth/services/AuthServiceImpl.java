package grublab.rest.auth.services;

import grublab.rest.auth.models.Admin;
import grublab.rest.auth.repositories.AdminRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static grublab.rest.auth.constants.SecurityConstants.KEY;

@Service
@Setter
public class AuthServiceImpl implements AuthService {

    private AdminRepository adminRepository;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthServiceImpl(AdminRepository adminRepository,
                           PasswordEncoder passwordEncoder) {
        this.adminRepository = adminRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void register(Admin admin) {
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        adminRepository.save(admin);
    }

    @Override
    public Admin convertTokenToAdmin(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        if (!token.equals("")) {
            Claims claims = Jwts.parser()
                    .setSigningKey(Keys.hmacShaKeyFor(KEY.getBytes()))
                    .parseClaimsJws(token)
                    .getBody();
            if (claims.getSubject() != null) {
                return adminRepository.findByUsername(claims.getSubject());
            }
        }
        return null;
    }

}
