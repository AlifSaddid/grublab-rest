package grublab.rest.auth.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import grublab.rest.auth.constants.SecurityConstants;
import grublab.rest.auth.models.Admin;
import grublab.rest.auth.services.AuthService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Key;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    AuthService authService;

    private Admin admin;

    private SecurityConstants securityConstants;

    @BeforeEach
    void setUp() {
        admin = new Admin();
        admin.setId(1);
        admin.setUsername("admin");
        admin.setPassword("admin");

        securityConstants = new SecurityConstants();
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJwtToken() {
        Date expired = new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);
        Key key = Keys.hmacShaKeyFor(SecurityConstants.KEY.getBytes());
        Claims claims = Jwts.claims()
                .setSubject(admin.getUsername());
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expired)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    @Test
    void testControllerRegisterAdmin() throws Exception {
        doNothing().when(authService).register(admin);
        mockMvc.perform(
                post("/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(admin))
        ).andExpect(status().isOk());
    }

    @Test
    void testControllerAuth() throws Exception {
        when(authService.convertTokenToAdmin(any())).thenReturn(admin);
        mockMvc.perform(
                post("/auth")
                .header("Authorization", "Bearer " + getJwtToken())
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1))
        .andExpect(jsonPath("$.username").value("admin"));
    }

    @Test
    void testControllerAuthFalseToken() throws Exception {
        when(authService.convertTokenToAdmin(any())).thenReturn(null);
        mockMvc.perform(
                post("/auth")
                .header("Authorization", "Bearer " + getJwtToken())
        ).andExpect(status().isUnauthorized());
    }

    @Test
    void testControllerAuthWithoutToken() throws Exception {
        mockMvc.perform(post("/auth")
        ).andExpect(status().isForbidden());
    }


}
