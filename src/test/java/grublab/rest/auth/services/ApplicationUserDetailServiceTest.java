package grublab.rest.auth.services;

import grublab.rest.auth.models.Admin;
import grublab.rest.auth.repositories.AdminRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ApplicationUserDetailServiceTest {

    @InjectMocks
    private ApplicationUserDetailService applicationUserDetailService;

    @Mock
    private AdminRepository adminRepository;

    private Admin admin;

    @BeforeEach
    void setUp() {
        admin = new Admin();
        admin.setUsername("admin");
        admin.setPassword("admin");
        admin.setId(1);

        applicationUserDetailService.setAdminRepository(adminRepository);
    }

    @Test
    void testLoadUserByUsernameShouldReturnUserDetails() {
        when(adminRepository.findByUsername("admin")).thenReturn(admin);

        UserDetails result = applicationUserDetailService.loadUserByUsername("admin");
        assertEquals("admin", result.getUsername());
    }

    @Test
    void testLoadUserByUsernameNotFound() {
        assertThrows(UsernameNotFoundException.class, () -> {
            applicationUserDetailService.loadUserByUsername("not a username");
        });
    }

}
