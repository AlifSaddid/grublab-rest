package grublab.rest.auth.services;

import grublab.rest.auth.constants.SecurityConstants;
import grublab.rest.auth.models.Admin;
import grublab.rest.auth.repositories.AdminRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.Key;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthServiceImplTest {

    @InjectMocks
    private AuthServiceImpl authService;

    @Mock
    AdminRepository adminRepository;

    private Admin admin;

    @BeforeEach
    void setUp() {
        authService.setAdminRepository(adminRepository);
        authService.setPasswordEncoder(new BCryptPasswordEncoder());

        admin = new Admin();
        admin.setId(1);
        admin.setUsername("admin");
        admin.setPassword("admin");
    }

    @Test
    void testRegisterShouldSaveAdmin() {
        final Admin repository = new Admin();

        doAnswer(invocation -> {
            repository.setPassword(admin.getPassword());
            repository.setId(admin.getId());
            repository.setPassword(admin.getPassword());
            return null;
        }).when(adminRepository).save(admin);

        authService.register(admin);
        assertEquals(repository.getPassword(), admin.getPassword());
    }

    @Test
    void testConvertTokenToAdmin() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        Date expired = new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);
        Key key = Keys.hmacShaKeyFor(SecurityConstants.KEY.getBytes());
        Claims claims = Jwts.claims()
                .setSubject(admin.getUsername());
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expired)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();

        request.addHeader(SecurityConstants.HEADER_NAME, "Bearer " + token);
        when(adminRepository.findByUsername("admin")).thenReturn(admin);
        Admin result = authService.convertTokenToAdmin(request);
        assertNotNull(result);
        assertEquals("admin", result.getUsername());
    }

    @Test
    void testConvertTokenToAdminWithoutToken() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(SecurityConstants.HEADER_NAME, "Bearer ");
        Admin result = authService.convertTokenToAdmin(request);
        assertNull(result);
    }
}
