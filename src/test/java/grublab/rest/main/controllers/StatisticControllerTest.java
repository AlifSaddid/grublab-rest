package grublab.rest.main.controllers;

import grublab.rest.auth.constants.SecurityConstants;
import grublab.rest.auth.models.Admin;
import grublab.rest.main.services.StatisticService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Key;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class StatisticControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    StatisticService statisticService;

    private Admin admin;

    @BeforeEach
    void setUp() {
        admin = new Admin();
        admin.setId(1);
        admin.setUsername("admin");
        admin.setPassword("admin");
    }

    private String getJwtToken() {
        Date expired = new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);
        Key key = Keys.hmacShaKeyFor(SecurityConstants.KEY.getBytes());
        Claims claims = Jwts.claims()
                .setSubject(admin.getUsername());
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expired)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    @Test
    void testGetPendapatanBulanIniShouldReturnInteger() throws Exception {
        when(statisticService.getPendapatanBulanIni()).thenReturn(20000);
        mockMvc.perform(
                get("/v1/statistics/pendapatan/current-month/")
                .header("Authorization", "Bearer " + getJwtToken())
        ).andExpect(jsonPath("$").value(20000));
    }

    @Test
    void testGetPendapatanTotalShouldReturnInteger() throws Exception {
        when(statisticService.getPendapatanTotal()).thenReturn(30000);
        mockMvc.perform(
                get("/v1/statistics/pendapatan/total/")
                        .header("Authorization", "Bearer " + getJwtToken())
        ).andExpect(jsonPath("$").value(30000));
    }

}
