package grublab.rest.main.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import grublab.rest.auth.constants.SecurityConstants;
import grublab.rest.auth.models.Admin;
import grublab.rest.main.models.Customer;
import grublab.rest.main.models.Menu;
import grublab.rest.main.models.Pesanan;
import grublab.rest.main.services.PesananService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class PesananControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PesananService pesananService;

    private Menu ayam;

    private Menu bebek;

    private Customer customer;

    private Pesanan pesanan;

    private Admin admin;

    @BeforeEach
    void setUp() {
        ayam = new Menu();
        ayam.setMenuId(1);
        ayam.setName("Ayam Goreng");

        bebek = new Menu();
        bebek.setMenuId(2);
        bebek.setName("Bebek Goreng");

        customer = new Customer();
        customer.setId(1);
        customer.setIdLine("alifsaddid15");

        pesanan = new Pesanan();
        pesanan.addMenu(ayam);
        pesanan.addMenu(bebek);

        ayam.setOrders(new ArrayList<>());
        bebek.setOrders(new ArrayList<>());
        ayam.addOrders(pesanan);
        bebek.addOrders(pesanan);

        admin = new Admin();
        admin.setId(1);
        admin.setUsername("admin");
        admin.setPassword("admin");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJwtToken() {
        Date expired = new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);
        Key key = Keys.hmacShaKeyFor(SecurityConstants.KEY.getBytes());
        Claims claims = Jwts.claims()
                .setSubject(admin.getUsername());
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expired)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    @Test
    void testControllerPostPesanan() throws Exception {
        when(pesananService.createPesanan(any(), any())).thenReturn(pesanan);
        mockMvc.perform(
                post("/v1/orders/alifsaddid15")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(pesanan))
        ).andExpect(jsonPath("$.pesananId").value("0"))
        .andExpect(jsonPath("$.menus[0].name").value("Ayam Goreng"))
        .andExpect(jsonPath("$.menus[1].name").value("Bebek Goreng"));
    }

    @Test
    void testControllerGetHistories() throws Exception {
        List<Pesanan> orders = new ArrayList<>();
        orders.add(pesanan);
        when(pesananService.getPesananByIdLine(customer.getIdLine())).thenReturn(orders);

        mockMvc.perform(
                get("/v1/orders/alifsaddid15")
        ).andExpect(jsonPath("$[0].menus[0].name").value("Ayam Goreng"));
    }

    @Test
    void testControllerGetAllOrders() throws Exception {
        List<Pesanan> orders = new ArrayList<>();
        orders.add(pesanan);
        when(pesananService.getAllPesanan()).thenReturn(orders);

        mockMvc.perform(
                get("/v1/orders/")
                .header("Authorization", "Bearer " + getJwtToken())
        ).andExpect(jsonPath("$[0].menus[0].name").value("Ayam Goreng"));
    }

}
