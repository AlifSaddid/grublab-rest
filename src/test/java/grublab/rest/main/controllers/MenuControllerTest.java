package grublab.rest.main.controllers;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import grublab.rest.auth.constants.SecurityConstants;
import grublab.rest.auth.models.Admin;
import grublab.rest.main.models.Menu;
import grublab.rest.main.services.MenuServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class MenuControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MenuServiceImpl menuService;

    private Menu menu;

    private Menu menu2;

    private Admin admin;

    @BeforeEach
    void setUp() {
        menu = new Menu();
        menu.setName("Matcha Latte");
        menu.setDescription("Matcha X Latte");
        menu.setPrice(49900);

        menu2 = new Menu();
        menu2.setName("Vanilla Latte");
        menu2.setDescription("Vanilla X Latte");
        menu2.setPrice(49900);

        admin = new Admin();
        admin.setId(1);
        admin.setUsername("admin");
        admin.setPassword("admin");

    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJwtToken() {
        Date expired = new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);
        Key key = Keys.hmacShaKeyFor(SecurityConstants.KEY.getBytes());
        Claims claims = Jwts.claims()
                .setSubject(admin.getUsername());
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expired)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    @Test
    void testControllerPostMenu() throws Exception {
        when(menuService.createMenu(any())).thenReturn(menu);
        mockMvc.perform(
                post("/v1/menus/")
                        .header("Authorization", "Bearer " + getJwtToken())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapToJson(menu)))
                .andExpect(jsonPath("$.name").value("Matcha Latte"));
    }

    @Test
    void testControllerGetAllMenus() throws Exception {
        List<Menu> menus = new ArrayList<>();
        menus.add(menu);
        menus.add(menu2);
        when(menuService.getAllMenus()).thenReturn(menus);
        mockMvc.perform(
                get("/v1/menus/").contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[0].name").value("Matcha Latte"))
                    .andExpect(jsonPath("$[1].name").value("Vanilla Latte"));
    }

    @Test
    void testControllerUpdateMenuById() throws Exception {
        menuService.createMenu(menu);
        menu.setName("Hazelnut Latte");

        when(menuService.updateMenuById(menu, menu.getMenuId())).thenReturn(menu);

        mockMvc.perform(
                put("/v1/menus/0")
                        .header("Authorization", "Bearer " + getJwtToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(menu)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Hazelnut Latte"));
    }

    @Test
    void testControllerGetMenuByName() throws Exception {
        when(menuService.getMenuByName(menu.getName())).thenReturn(menu);

        mockMvc.perform(get("/v1/menus/" + menu.getName())
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.name").value(menu.getName()));
    }

    @Test
    void testControllerGetMenuByNameNotFound() throws Exception {
        when(menuService.getMenuByName(menu.getName())).thenReturn(null);

        mockMvc.perform(get("/v1/menus/" + menu.getName())
        ).andExpect(status().isNotFound());
    }

    @Test
    void testControllerDeleteMenuShouldReturnOk() throws Exception {
        doNothing().when(menuService).deleteMenuById(menu.getMenuId());

        mockMvc.perform(post("/v1/menus/" + menu.getMenuId())
                .header("Authorization", "Bearer " + getJwtToken())
        ).andExpect(status().isNoContent());
    }
}
