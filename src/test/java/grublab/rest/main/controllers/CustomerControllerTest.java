package grublab.rest.main.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import grublab.rest.auth.constants.SecurityConstants;
import grublab.rest.auth.models.Admin;
import grublab.rest.main.models.Customer;
import grublab.rest.main.services.CustomerService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    private Customer customerA;

    private Customer customerB;

    private List<Customer> customers;

    private Admin admin;

    @BeforeEach
    public void setUp() {
        customerA = new Customer();
        customerA.setIdLine("A");
        customerA.setPoint(0);

        customerB = new Customer();
        customerB.setIdLine("B");
        customerB.setPoint(100);

        customers = new ArrayList<>();
        customers.add(customerA);
        customers.add(customerB);

        admin = new Admin();
        admin.setId(1);
        admin.setUsername("admin");
        admin.setPassword("admin");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJwtToken() {
        Date expired = new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME);
        Key key = Keys.hmacShaKeyFor(SecurityConstants.KEY.getBytes());
        Claims claims = Jwts.claims()
                .setSubject(admin.getUsername());
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expired)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    @Test
    void testCreateCustomerShouldCreateNewCustomer() throws Exception {
        when(customerService.createCustomer(any())).thenReturn(customerA);

        mockMvc.perform(post("/v1/customers/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(customerA))
        ).andExpect(jsonPath("$.idLine").value("A"));
    }

    @Test
    void testGetCustomerByIdLineShouldReturnCustomer() throws Exception {
        when(customerService.getCustomerByIdLine("A")).thenReturn(customerA);

        mockMvc.perform(get("/v1/customers/A")
        ).andExpect(jsonPath("$.idLine").value("A"));
    }

    @Test
    void testUpdateCustomerByIdLineShouldReturnCustomer() throws Exception {
        customerService.createCustomer(customerA);
        customerA.setPoint(100);

        when(customerService.updateCustomerPoint(any(), any())).thenReturn(customerA);

        mockMvc.perform(put("/v1/customers/A")
                .header("Authorization", "Bearer " + getJwtToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(customerA))
        ).andExpect(jsonPath("$.point").value(100));
    }

    @Test
    void testGetAllCustomersShouldReturnAllCustomers() throws Exception {
        when(customerService.getAllCustomers()).thenReturn(customers);

        mockMvc.perform(get("/v1/customers/")
                .header("Authorization", "Bearer " + getJwtToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(customerB))
        ).andExpect(jsonPath("$[0].idLine").value("A"))
        .andExpect(jsonPath("$[1].idLine").value("B"));
    }

    @Test
    void testGetCustomerByIdLineNotFound() throws Exception {
        when(customerService.getCustomerByIdLine("A")).thenReturn(null);

        mockMvc.perform(get("/v1/customers/A")
        ).andExpect(status().isNotFound());
    }

}
