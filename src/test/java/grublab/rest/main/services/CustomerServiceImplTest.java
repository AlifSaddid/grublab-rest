package grublab.rest.main.services;

import grublab.rest.main.models.Customer;
import grublab.rest.main.repositories.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Mock
    private CustomerRepository customerRepository;

    private Customer customerA;

    private Customer customerB;

    private List<Customer> customers;

    @BeforeEach
    void setUp() {
        customerService.setCustomerRepository(customerRepository);

        customerA = new Customer();
        customerA.setIdLine("A");
        customerA.setPoint(0);

        customerB = new Customer();
        customerB.setIdLine("B");
        customerB.setPoint(100);

        customers = new ArrayList<>();
        customers.add(customerA);
        customers.add(customerB);
    }

    @Test
    void testCreateCustomerShouldReturnCustomer() {
        lenient().when(customerRepository.save(customerA)).thenReturn(null);

        Customer result = customerService.createCustomer(customerA);
        assertEquals(customerA.getIdLine(), result.getIdLine());
    }

    @Test
    void testGetCustomerByIdLineShouldReturnCustomer() {
        lenient().when(customerRepository.findByIdLine("A")).thenReturn(customerA);

        Customer result = customerService.getCustomerByIdLine(customerA.getIdLine());
        assertEquals(customerA.getIdLine(), result.getIdLine());
    }

    @Test
    void testUpdateCustomerPointShouldReturnNewPoint() {
        lenient().when(customerRepository.findByIdLine("A")).thenReturn(customerA);
        lenient().when(customerRepository.save(customerA)).thenReturn(null);

        Customer result = customerService.updateCustomerPoint(customerB, customerA.getIdLine());
        assertEquals(customerB.getPoint(), result.getPoint());
    }

    @Test
    void testGetAllCustomerShouldReturnAllCustomers() {
        when(customerRepository.findAll()).thenReturn(customers);
        List<Customer> result = customerService.getAllCustomers();

        assertEquals(2, result.size());
    }

}
