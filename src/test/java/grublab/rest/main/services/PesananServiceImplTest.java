package grublab.rest.main.services;

import grublab.rest.main.models.Customer;
import grublab.rest.main.models.Menu;
import grublab.rest.main.models.Pesanan;
import grublab.rest.main.repositories.PesananRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PesananServiceImplTest {

    @InjectMocks
    private PesananServiceImpl pesananService;

    @Mock
    private PesananRepository pesananRepository;

    @Mock
    private CustomerService customerService;

    @Mock
    private MenuService menuService;

    private Menu ayam;

    private Menu bebek;

    private Customer customer;

    private Pesanan pesanan;

    @BeforeEach
    void setUp() {
        ayam = new Menu();
        ayam.setMenuId(1);
        ayam.setName("Ayam Goreng");

        bebek = new Menu();
        bebek.setMenuId(2);
        bebek.setName("Bebek Goreng");

        customer = new Customer();
        customer.setId(1);
        customer.setIdLine("alifsaddid15");

        pesanan = new Pesanan();
        pesanan.addMenu(ayam);
        pesanan.addMenu(bebek);
    }

    @Test
    void testCreatePesananShouldAddPesanan() {
        when(menuService.getMenuByName("Ayam Goreng")).thenReturn(ayam);
        when(menuService.getMenuByName("Bebek Goreng")).thenReturn(bebek);
        when(customerService.getCustomerByIdLine("alifsaddid15")).thenReturn(customer);

        final Pesanan repository = new Pesanan();

        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            Pesanan pesananArgs =  (Pesanan)args[0];
            repository.setMenus(pesananArgs.getMenus());
            return null;
        }).when(pesananRepository).save(pesanan);

        Pesanan result = pesananService.createPesanan(customer.getIdLine(), pesanan);

        verify(pesananRepository, times(1)).save(any());
        assertEquals(2, result.getMenus().size());
    }

    @Test
    void testGetPesananByIdLineShouldReturnPesanan() {
        List<Pesanan> orders = new ArrayList<>();
        orders.add(pesanan);
        when(customerService.getCustomerByIdLine(customer.getIdLine())).thenReturn(customer);
        when(pesananRepository.getAllByCustomer(customer)).thenReturn(orders);

        Iterable<Pesanan> result = pesananService.getPesananByIdLine(customer.getIdLine());

        assertEquals(1, ((Collection<?>) result).size());
    }

    @Test
    public void testGetAllPesananShouldReturnAllPesanan() {
        List<Pesanan> orders = new ArrayList<>();
        orders.add(pesanan);
        when(pesananRepository.findAll()).thenReturn(orders);

        Iterable<Pesanan> result = pesananService.getAllPesanan();
        assertEquals(1, ((Collection<?>) result).size());
    }

}
