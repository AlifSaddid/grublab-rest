package grublab.rest.main.service;

import grublab.rest.main.models.Menu;
import grublab.rest.main.repositories.MenuRepository;
import grublab.rest.main.services.MenuServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MenuServiceTest {
    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuServiceImpl menuService;

    private Menu menu;
    private Menu menu2;

    @BeforeEach
    public void setUp(){
        menu = new Menu();
        menu.setName("Matcha Latte");
        menu.setDescription("Matcha X Latte");
        menu.setPrice(49900);

        menu2 = new Menu();
        menu2.setName("Vanilla Latte");
        menu2.setDescription("Vanilla X Latte");
        menu2.setPrice(49900);
    }

    @Test
    void testServiceCreateMenu(){
        menuService.createMenu(menu);
        verify(menuRepository, times(1)).save(menu);
    }

    @Test
    void testServiceGetAllMenus(){
        menuService.createMenu(menu);
        menuService.createMenu(menu2);

        Iterable<Menu> menus = menuRepository.findAll();
        lenient().when(menuService.getAllMenus()).thenReturn(menus);
        Iterable<Menu> menuFromService = menuService.getAllMenus();

        assertIterableEquals(menus, menuFromService);
    }

    @Test
    void testServiceGetMenuByName() {
        menuService.createMenu(menu);

        when(menuService.getMenuByName("Matcha Latte")).thenReturn(menu);
        Menu menuRes = menuService.getMenuByName(menu.getName());

        assertEquals(menu.getName(), menuRes.getName());
    }

    @Test
    void testServiceDeleteMenuById() {
        menuService.createMenu(menu);
        menuService.deleteMenuById(menu.getMenuId());

        lenient().when(menuService.getMenuByName(menu.getName())).thenReturn(null);
        assertNull(menuService.getMenuByName(menu.getName()));
    }

    @Test
    void testServiceUpdateMenuById(){
        menuService.createMenu(menu);
        String preUpdateName = menu.getName();
        menu.setName("Hazelnut Latte");

        lenient().when(menuService.updateMenuById(menu, menu.getMenuId())).thenReturn(menu);
        Menu postUpdateMenu = menuService.updateMenuById(menu, menu.getMenuId());

        assertNotEquals(postUpdateMenu.getName(), preUpdateName);
        assertEquals(postUpdateMenu.getPrice(), menu.getPrice());
    }
}
