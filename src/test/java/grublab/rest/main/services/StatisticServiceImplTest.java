package grublab.rest.main.services;

import grublab.rest.main.models.Customer;
import grublab.rest.main.models.Menu;
import grublab.rest.main.models.Pesanan;
import grublab.rest.main.repositories.PesananRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StatisticServiceImplTest {

    @InjectMocks
    private StatisticServiceImpl statisticService;

    @Mock
    private PesananRepository pesananRepository;

    List<Pesanan> orders;

    private Menu ayam;

    private Menu bebek;

    private Customer customer;

    private Pesanan pesanan;

    private Pesanan pesanan2;

    @BeforeEach
    void setUp() {
        ayam = new Menu();
        ayam.setMenuId(1);
        ayam.setName("Ayam Goreng");
        ayam.setPrice(10000);

        bebek = new Menu();
        bebek.setMenuId(2);
        bebek.setName("Bebek Goreng");
        bebek.setPrice(10000);

        customer = new Customer();
        customer.setId(1);
        customer.setIdLine("alifsaddid15");

        pesanan = new Pesanan();
        pesanan.addMenu(ayam);
        pesanan.addMenu(bebek);
        pesanan.setDate(LocalDateTime.now());

        orders = new ArrayList<>();
        orders.add(pesanan);

        pesanan2 = new Pesanan();
        pesanan2.addMenu(ayam);
        pesanan2.setDate(LocalDateTime.now().minusDays(31));

        orders.add(pesanan2);
    }

    @Test
    void testGetPendapatanBulanIniShouldReturnCorrectSum() {
        when(pesananRepository.findAll()).thenReturn(orders);

        Integer result = statisticService.getPendapatanBulanIni();
        assertEquals(20000, result);
    }

    @Test
    void testGetPendapatanShouldReturnCorrectSum() {
        when(pesananRepository.findAll()).thenReturn(orders);

        Integer result = statisticService.getPendapatanTotal();
        assertEquals(30000, result);
    }

}
